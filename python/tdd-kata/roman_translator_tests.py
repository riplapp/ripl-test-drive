import unittest

from roman_translator import RomanTranslator


class RomanTranslatorTestCase(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.roman_translator = RomanTranslator()

    def test_translate_one(self):
        self.assertEqual(1, self.roman_translator.translate("I"))


if __name__ == '__main__':
    unittest.main()
