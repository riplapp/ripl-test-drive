package kata.roman;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RomanTranslatorTests
{

  private RomanTranslator translator;

  @Before
  public void setup()
  {

    translator = new RomanTranslator();
  }

  @Test
  public void testTranslateOne()
  {

    Assert.assertEquals( 1, translator.translate( "I" ) );
  }

}
