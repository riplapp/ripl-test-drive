require './roman_translator'

describe 'Roman Translator Should do' do
  before(:each) do
    @translator = RomanTranslator.new
  end
  it 'should translate I to 1' do
    expect(@translator.translate("I")).to eq(1)
  end
end