require './roman-kata/roman_translator'
require "test/unit"

class TestRomanTranslator < Test::Unit::TestCase
  def setup
    @translator = RomanTranslator.new
  end

  def test_translate_I
    assert_equal(1, @translator.translate("I"))
  end

end