
# Roman Numerals Kata

## Roman numbering

The Romans wrote numbers using letters : I, V, X, L, C, D, M.

| Digit | Value |
| ---- | ---- |
| I | 1 |
| V | 5 |
| X | 10 |
| L | 50 |
| C | 100 |
| D | 500 |
| M | 1000 |


**Rules**

* Except when subtraction is indicated, Roman numbers start at the highest symbol and end at the lowest.
* The value of the Roman number is the sum of the value of each digit, except when subtraction is indicated.
    * XI = 11
* Any smaller digit in front of any larger digit indicates subtraction.
    * IX = 9
* A symbol can be repeated a maximum of three times. Where a symbol would be repeated a fourth time, use subtraction against a higher value instead.
    * For 4, use IV instead of IIII.
* Where non-zero, the ones, tens, hundreds, and thousands values must be separately shown.
    * For 99, use XCIX instead of IC.
    * For 2999, use MMCMXCIX instead of MMIM.


**Examples**

* III = 3
* VI = 6
* XII = 12
* XV = 15
* XXIV = 24
* XL = 40
* XC = 90
* LXXV = 75 
* CX = 110  
* CDXLVIII = 448
* MCXIV = 1114  
* MMDCCLI = 2751
* MMCMXCIII = 2993  


### Part 1

Write a function to convert from Roman number to Arabic number. The function will accept a string and return an integer. The string may only contain the Roman numbering digits.


### Part 2

Write a function to convert the other direction: from Arabic numbers to Roman numbers. The function will accept a positive (non-zero) integer and return a string. 

If it proves problematic, there is no need to be able to convert numbers larger than about 3000.


## Questions that might have interesting answers

* Can you make the code really beautiful and highly readable?
* Does it help to break out lots of small named functions from the main function, or is it better to keep it all in one function?
* If you don't know an algorithm to do this already, can you derive one using strict TDD?
* Does the order you take the tests in affect the final design of your algorithm?
* Would it be better to work out an algorithm first before starting with TDD?
* If you do know an algorithm already, can you implement it using strict TDD?
* Can you think of another algorithm?
* What are the best data structures for storing all the numeral letters? (I, V, D, M etc)
* Can you define the test cases in xUnit?
* What is the best way to verify your tests are correct?


## References

* Coding Dojo Roman Numerals Kata: 
    * https://codingdojo.org/kata/RomanNumerals/
* Description of Roman numbering system: 
    * http://www.novaroma.org/via_romana/numbers.html
    * Includes Javscript calculator
* Another kata description which includes nuanced details stemming from how Roman numbering works: 
    * https://www.thevbprogrammer.com/VB2010_08/08-10-RomanNumerals.htm
